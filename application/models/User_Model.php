<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model
{
	private $tableName = 'users';
	private $alias = 'US';

	public $validationRules = [
		[
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'required|min_length[3]|max_length[50]'
		],
		[
			'field' => 'surname',
			'label' => 'Surname',
			'rules' => 'required|min_length[3]|max_length[50]'
		],
		[
			'field' => 'login',
			'label' => 'Login',
			'rules' => 'required|min_length[3]|max_length[50]'
		],
		[
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_email'
		]
	];

	/**
	 * Validation rules
	 * @return $this->validationRules
	 */
	public function getValidationRules()
	{
		return $this->validationRules;
	}

	/**
	 * Add record
	 * @param array $data
	 * @return  $this->db->insert()
	 */
	public function add($data = [])
	{
		return $this->db->insert($this->tableName, $data);
	}

	/**
	 * Update record
	 * @param array $conditions
	 * @param $data
	 * @return  $this->db->update()
	 */
	public function update($conditions = [], $data)
	{
		extract($conditions);

		$this->db = $this->conditions($conditions);

		return $this->db->update($this->tableName, $data);
	}

	/**
	 * Delete record
	 * @param array $conditions
	 * @return  $this->db->delete()
	 */
	public function delete($conditions = [])
	{
		extract($conditions);

		$this->db = $this->conditions($conditions);

		return $this->db->delete($this->tableName);
	}

	/**
	 * Get all record
	 * @param array $conditions
	 * @return $this->db->get()->result_array()
	 */
	public function getAll($conditions = [])
	{
		$orderCol = 'date_add';
		$orderDir = 'DESC';
		$column = $this->alias . '.*';

		extract($conditions);

		$this->db->select($column);
		$this->db = $this->conditions($conditions);

		$this->db->order_by($this->alias . '.' . $orderCol, $orderDir);

		if (!empty($limit) && !empty($offset)) {
			$this->db->limit($limit, $offset);
		}

		return $this->db->get($this->tableName . ' AS ' . $this->alias)->result_array();
	}

	/**
	 * Get one record
	 * @param array $conditions
	 * @return $this->db->row_array()
	 */
	public function getOne($conditions = [])
	{
		$column = $this->alias . '.*';

		extract($conditions);

		$this->db->select($column);
		$this->db = $this->conditions($conditions);

		return $this->db->get($this->tableName . ' AS ' . $this->alias)->row_array();
	}

	/**
	 * Count records
	 * @param array $conditions
	 * @return $this->db->count_all_results()
	 */
	public function count($conditions = [])
	{
		$this->db->from($this->tableName);
		$this->db = $this->conditions($conditions);

		return $this->db->count_all_results($this->tableName . ' AS ' . $this->alias);
	}

	/**
	 * Check if record exists
	 * @param int $id
	 * @return $this->db->count_all_results()
	 */
	public function checkExists($id = 0)
	{
		$this->db->from($this->tableName);
		$this->db->where('id_user', $id);
		return $this->db->count_all_results();
	}

	/**
	 * Generate all conditions
	 * @param array $conditions
	 * @return $this->db
	 */
	private function conditions($conditions = [])
	{
		extract($conditions);

		if (isset($id_user)) {
			$this->db->where('id_user', $id_user);
		}

		if (isset($login)) {
			$this->db->where('login', $login);
		}

		if (isset($keywords)) {
			$this->db->like('name', $keywords, 'both');
		}

		return $this->db;
	}
}
