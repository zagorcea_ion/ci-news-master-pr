<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('User');

		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
		}
	}

	/**
     * Display admin news page
     */
    public function index()
    {
        $this->load->view('layouts/admin-header', ['activePage' => 'news']);
        $this->load->view('admin/news/index');
        $this->load->view('layouts/admin-footer');
    }

    /**
     * Select all records for Data Table plugin
     */
    public function dtNews()
    {
		checkAjax();

        $columns = [
            0 => 'title',
            3 => 'date_add'
        ];

        $post = $this->input->post();
        $this->load->model('News_Model', 'news');

        //Config search data
        $search = $post['search']['value'];

        if (!empty($search)) {
            $params['keywords'] =  $search;
        }

        $params['offset'] = intval($post['start']);
        $params['limit'] = intval($post['length']);
        $params['orderDir'] = $post['order'][0]['dir'];
        $params['orderCol'] =  $columns[intval($post['order'][0]['column'])];
        $params['join_categories'] = true;

        // debug($params);die;
        $news = $this->news->getAll($params);

        $data = [];

        if (!empty($news)) {
            foreach ($news as $key => $item) {
                $data[$key] = [
                    'title'        => $item['title'],
                    'category'     => $item['cat_name'],
                    'image'        => '<img src="' . base_url('public/images/news/') . $item['image'] . '" width="100px">',
                    'is_published' => $this->load->view('admin/news/partial/is_published', $item, true),
					'is_must_read' => $this->load->view('admin/news/partial/is_must_read', $item, true),
                    'date_add'     => date('d.m.Y', strtotime($item['date_add'])),
                    'action'       => $this->load->view('admin/news/partial/action', $item, true),
                ];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = $this->news->count();
        $output['recordsFiltered'] = $this->news->count($params);

        echo json_encode($output);
    }

	/**
	 * Display admin news form
	 * @param int $id
	 */
    public function form($id = 0)
    {
        //load Model
        $this->load->model('Category_Model', 'category');

        $data = [
            'categories' => $categories = $this->category->getAll(),
            'activePage' => 'news'
        ];

        if (!empty($id)) {
            $this->load->model('News_Model', 'news');
            $data['record'] = $this->news->getOne(['id_news' => $id]);
        }

        $this->load->view('layouts/admin-header', $data);
        $this->load->view('admin/news/form');
        $this->load->view('layouts/admin-footer');
    }

    /**
     * Store news
     */
    public function store()
    {
		checkAjax();

        $post = $this->input->post(null, true);

        $this->load->library('form_validation');
        $this->load->model('News_Model', 'news');

        $this->form_validation->set_rules($this->news->getValidationRules());
        if(!$this->form_validation->run()){
            displayJson('error', validation_errors());
        }

        $data = [
            'title'        => $post['title'],
			'alias' 	   => makeSlugs($post['title']),
            'description'  => $post['description'],
            'id_category'  => $post['id_category'],
            'date_publish' => $post['date_publish'],
            'content'      => $post['content'],
			'is_published' => !isset($post['is_published']) ? 0 : 1,
			'is_must_read' => !isset($post['is_must_read']) ? 0 : 1,
        ];

        if (!empty($_FILES['image']['name'])) {
            $this->load->helper('My_file');

            $data['image'] = saveImage('public/images/news', 900, 500);

            if (!empty($post['old_image'])) {
                deleteFile('public/images/news/' . $post['old_image']);
            }
        }

        if (!empty($post['id_news'])) {

            $id = intval($post['id_news']);

            if (!$this->news->checkExists($id)) {
                displayJson('error', 'News was not found');
            }

            if (!$this->news->update(['id_news' => $id], $data)) {
                displayJson('success', 'News was updated successfully');
            }

            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'News was updated successfully');

            displayJson('success', 'News was updated successfully', ['redirect' => base_url('admin-panel/news')]);

        } elseif (!isset($post['id_news'])) {
            $data['date_add'] = date('Y-m-d H:i:s');

			if (!$this->news->add($data)) {
                displayJson('error', 'News was not added successfully');
            }

            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'News was added successfully');

            displayJson('success', 'News was added successfully', ['redirect' => base_url('admin-panel/news')]);
        }
    }

	/**
	 * Delete news
	 * @params $id id record
	 * @param int $id
	 */
    public function delete($id = 0)
    {
		checkAjax();

        $id = intval($id);

        $this->load->model('News_Model', 'news');

        $news = $this->news->getOne(['id_news' => $id]);

        if (empty($news)) {
            displayJson('error', 'News was not found');
        }

        if (!empty($news['image'])) {
            $this->load->helper('My_file');
            deleteFile('public/images/news/' . $news['image']);
        }

        if (!$this->news->delete(['id_news' => $id])) {
            displayJson('error', 'News was not delete successfully');
        }

        displayJson('success', 'News was delete successfully');
    }

	/**
	 * Make news published
	 * @param int $id
	 */
	public function isPublished($id = 0)
	{
		checkAjax();

		$id = intval($id);

		//load Model
		$this->load->model('News_Model', 'news');

		$news = $this->news->getOne(['id_news' => $id]);

		if (empty($news)) {
			displayJson('error', 'News was not found');
		}

		if ($news['is_published']) {
			$data['is_published'] = 0;
			$message = 'News was unpublished successfully';

		} else {
			$data['is_published'] = 1;
			$message = 'News was published successfully';
		}

		if ($this->news->update(['id_news' => $id], $data)) {
			displayJson('success', $message);
		}
	}

	/**
	 * Make news must read
	 * @param int $id
	 */
	public function isMustRead($id = 0)
	{
		checkAjax();

		$id = intval($id);

		//load Model
		$this->load->model('News_Model', 'news');

		$news = $this->news->getOne(['id_news' => $id]);

		if (empty($news)) {
			displayJson('error', 'News was not found');
		}

		if ($news['is_must_read']) {
			$data['is_must_read'] = 0;
			$message = 'News is not "must read"';

		} else {
			$data['is_must_read'] = 1;
			$message = 'News was made "must reade"';
		}

		if ($this->news->update(['id_news' => $id], $data)) {
			displayJson('success', $message);
		}
	}
}
