<a data-fancybox data-type="ajax" data-src="<?= base_url('admin-panel/categories/form/' . $id_cat) ?>" href="javascript:;" class="btn btn-primary">
	<i class="fas fa-edit"></i> Edit
</a>
<button type="button" class="btn btn-danger confirm" data-text="Do you want delete this category?" data-url="<?= base_url('admin-panel/categories/delete/' . $id_cat) ?>">
	<i class="fas fa-trash-alt"></i> Delete
</button>
