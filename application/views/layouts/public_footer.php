<!-- Start footer area -->
<footer id="footer" class="footer-wrapper footer-1">
	<!-- Start footer top area -->
	<div class="footer-top-wrap ptb-70 bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-5 hidden-sm">
					<div class="zm-widget pr-40">
						<h2 class="h6 zm-widget-title uppercase text-white mb-30">About Znews</h2>
						<div class="zm-widget-content">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-6 col-lg-3">
					<div class="zm-widget">
						<h2 class="h6 zm-widget-title uppercase text-white mb-30">Social Links</h2>
						<div class="zm-widget-content">
							<div class="zm-social-media zm-social-1">
								<ul>
									<li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i>Like us on Facebook</a></li>
									<li><a href="https://twitter.com"><i class="fa fa-twitter"></i>Tweet us on Twitter</a></li>
									<li><a href="https://www.pinterest.com/"><i class="fa fa-pinterest"></i>Pin us on Pinterest</a></li>
									<li><a href="https://www.instagram.com/"><i class="fa fa-instagram"></i>Heart us on Instagram</a></li>
									<li><a href="https://plus.google.com"><i class="fa fa-google-plus"></i>Share us on GooglePlus</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
		
				<div class="col-xs-12 col-sm-4 col-md-6 col-lg-4">
					<div class="zm-widget">
						<h2 class="h6 zm-widget-title uppercase text-white mb-30">Contact</h2>
						<!-- Start Subscribe From -->
						<div class="zm-widget-content">
							<div class="subscribe-form subscribe-footer">
								<p>If you have any questions you can contact us.</p>
								<a href="<?=base_url('contact')?>" class="contact-us"> Contact us</a>
							</div>
						</div>
						<!-- End Subscribe From -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End footer top area -->
	<div class="footer-buttom bg-black ptb-15">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="zm-copyright text-center">
						<p class="uppercase">© <?=date('Y')?> Your Company. All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- End footer area -->
</div>
<!-- Body main wrapper end -->

<!-- Placed js at the end of the document so the pages load faster -->
<!-- jquery latest version -->
<script src="<?=base_url('public/js/vendor/jquery-1.12.1.min.js')?>"></script>
<!-- Bootstrap framework js -->
<script src="<?=base_url('public/js/bootstrap.min.js')?>"></script>
<!-- All js plugins included in this file. -->
<script src="<?=base_url('public/js/owl.carousel.min.js')?>"></script>
<script src="<?=base_url('public/js/plugins.js')?>"></script>
<!-- Main js file that contents all jQuery plugins activation. -->
<script src="<?=base_url('public/js/main.js')?>"></script>

<script src="<?=base_url('public/AdminLTE/plugins/sweetalert2/sweetalert2.min.js')?>"></script>

<script src="<?=base_url('public/plugins/Validation-Engine/jquery.validationEngine.js')?>"></script>
<script src="<?=base_url('public/plugins/Validation-Engine/jquery.validationEngine-en.js')?>"></script>

</body>
</html>
