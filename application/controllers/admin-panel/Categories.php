<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('User');

		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
		}
	}

    /**
     * Display admin categories page
     */
    public function index()
    {
        $this->load->view('layouts/admin-header', ['activePage' => 'categories']);
        $this->load->view('admin/categories/index');
        $this->load->view('layouts/admin-footer');
    }

    /**
     * Select all records for Data Table plugin
     */
    public function dtCategories()
    {
		checkAjax();

        $columns = [
            0 => 'name',
            1 => 'date_add',
        ];

        $post = $this->input->post();
        $this->load->model('Category_Model', 'category');

        //Config search data
        $search = $post['search']['value'];

        if (!empty($search)) {
            $params['keywords'] =  $search;
        }

        $params['offset'] = intval($post['start']);
        $params['limit'] = intval($post['length']);
        $params['orderDir'] = $post['order'][0]['dir'];
        $params['orderCol'] =  $columns[intval($post['order'][0]['column'])];

        $category = $this->category->getAll($params);

        $data = [];

        if (!empty($category)) {
            foreach ($category as $key => $item) {
                $data[$key] = [
                    'name'     => $item['name'],
                    'date_add' => date('d.m.Y', strtotime($item['date_add'])),
                    'action'   => $this->load->view('admin/categories/partial/action', $item, true),
				];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = $this->category->count();
        $output['recordsFiltered'] = $this->category->count($params);

        echo json_encode($output);
    }

	/**
	 * Display admin categories form
	 * @param int $id
	 */
    public function form($id = 0)
    {
		checkAjax();

        if (!empty($id)) {
            $this->load->model('Category_Model', 'category');
            $category = $this->category->getOne(['id_cat' => $id]);

            $this->load->view('admin/categories/form', $category);
        }

        $this->load->view('admin/categories/form');
    }

    /**
     * Store category
     */
    public function store()
    {
		checkAjax();

        $post = $this->input->post(null, true);

        $this->load->library('form_validation');
        $this->load->model('Category_Model', 'category');

        $this->form_validation->set_rules($this->category->getValidationRules());
        if(!$this->form_validation->run()){
            displayJson('error', validation_errors());
        }

        $data = [
        	'name'  =>  $post['name'],
			'alias' => makeSlugs($post['name']),
		];

        if (!empty($post['id_cat'])) {

            $id = intval($post['id_cat']);

            if (!$this->category->checkExists($id)) {
                displayJson('error', 'Category was not found');
            }

            if (!$this->category->update(['id_cat' => $id], $data)) {
                displayJson('success', 'Category was not updated');
            }

            displayJson('success', 'Category was updated successfully');

        } elseif (!isset($post['id_cat'])) {
            $data['date_add'] = date('Y-m-d H:i:s');

            if (!$this->category->add($data)) {
                displayJson('error', 'Category was not added');
            }

            displayJson('success', 'Category was added successfully');
        }
    }

	/**
	 * Delete category
	 * @param int $id
	 */
    public function delete($id = 0)
    {
		checkAjax();

        $id = intval($id);

        $this->load->model('Category_Model', 'category');

        if (!$this->category->checkExists($id)) {
            displayJson('error', 'Category was not found');
        }

        if (!$this->category->delete(['id_cat' => $id])) {
            displayJson('error', 'Category was not delete successfully');
        }

        displayJson('success', 'Category was delete successfully');
    }
}
