<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_Model extends CI_Model
{
    private $tableName = 'news';
    private $tableJoin = 'categories';
    private $alias = 'NW';
    public $validationRules = [
        [
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'required|min_length[3]|max_length[100]'
        ],
		[
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'required|min_length[50]|max_length[255]'
		],
		[
			'field' => 'id_category',
			'label' => 'Category',
			'rules' => 'required'
		],
		[
			'field' => 'date_publish',
			'label' => 'Date publish',
			'rules' => 'required'
		],
		[
			'field' => 'content',
			'label' => 'Content',
			'rules' => 'required|min_length[50]'
		],
    ];

    /**
     * Validation rules
     * @return $this->validationRules
     */
    public function getValidationRules()
    {
        return $this->validationRules;
    }

	/**
	 * Add record
	 * @param array $data
	 * @return  $this->db->insert()
	 */
    public function add($data = [])
    {
        return $this->db->insert($this->tableName, $data);
    }

	/**
	 * Update record
	 * @param array $conditions
	 * @param $data
	 * @return  $this->db->update()
	 */
    public function update($conditions = [], $data)
    {
        extract($conditions);

        $this->db = $this->conditions($conditions);

        return $this->db->update($this->tableName, $data);
    }

	/**
	 * Delete record
	 * @param array $conditions
	 * @return  $this->db->delete()
	 */
    public function delete($conditions = [])
    {
        extract($conditions);

        $this->db = $this->conditions($conditions);

        return $this->db->delete($this->tableName);
    }

	/**
	 * Get all record
	 * @param array $conditions
	 * @return $this->db->get()->result_array()
	 */
    public function getAll($conditions = [])
    {
        $orderCol = 'date_add';
        $orderDir = 'DESC';
        $column = $this->alias . '.*';

        extract($conditions);

        $this->db->select($column);
        $this->db = $this->conditions($conditions);

        if (isset($order_random)) {
			$this->db->order_by('id_news', 'RANDOM');

		} else {
			$this->db->order_by($this->alias . '.' . $orderCol, $orderDir);
		}

        if (!empty($limit) && !empty($offset)) {
            $this->db->limit($limit, $offset);

        } elseif (!empty($limit)) {
			$this->db->limit($limit);
		}

        return $this->db->get($this->tableName . ' AS ' . $this->alias)->result_array();
    }

	/**
	 * Get one record
	 * @param array $conditions
	 * @return $this->db->row_array()
	 */
    public function getOne($conditions = [])
    {
        $column = $this->alias . '.*';

        extract($conditions);

        $this->db->select($column);
        $this->db = $this->conditions($conditions);

        return $this->db->get($this->tableName . ' AS ' . $this->alias)->row_array();
    }

	/**
	 * Count records
	 * @param array $conditions
	 * @return $this->db->count_all_results()
	 */
    public function count($conditions = [])
    {
        $this->db->from($this->tableName . ' AS ' . $this->alias);
        $this->db = $this->conditions($conditions);

        return $this->db->count_all_results();
    }

	/**
	 * Check if record exists
	 * @param int $id
	 * @return $this->db->count_all_results()
	 */
    public function checkExists($id = 0)
    {
        $this->db->from($this->tableName);
        $this->db->where('id_news', $id);
        return $this->db->count_all_results();
    }

	/**
	 * Increment views
	 * @param int $id
	 * @return $this->db->count_all_results()
	 */
    public function incrementViews($id = 0)
	{
		$this->db->where('id_news', $id);
		$this->db->set('views', 'views+1', FALSE);
		$this->db->update($this->tableName);
	}

	/**
	 * Generate all conditions
	 * @param array $conditions
	 * @return $this->db
	 */
    private function conditions($conditions = [])
    {
        extract($conditions);

        if (isset($join_categories) && $join_categories) {
            $this->db->select('CAT.name as cat_name, CAT.id_cat as id_cat, CAT.alias as cat_alias');
            $this->db->join($this->tableJoin . ' CAT', 'CAT.id_cat = ' . $this->alias . '.id_category');
        }

		if (isset($not_is)) {
			$this->db->where('id_news !=', $not_is);
        }
        
        if (isset($id_news)) {
            $this->db->where('id_news', $id_news);
        }
        
        if (isset($id_category)) {
            $this->db->where('id_category', $id_category);
        }
        

        if (isset($is_must_read) && $is_must_read) {
            $this->db->where('is_must_read', '1');
        }

        if (isset($keywords)) {
            $this->db->like('name', $keywords, 'both');
        }

        return $this->db;
    }
}
