<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.0.0-rc.1
    </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url('public/AdminLTE/plugins/jquery-ui/jquery-ui.min.js')?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('public/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- ChartJS -->

<script src="<?=base_url('public/AdminLTE/plugins/sweetalert2/sweetalert2.min.js')?>"></script>

<script src="<?=base_url('public/AdminLTE/plugins/chart.js/Chart.min.js')?>"></script>
<!-- Sparkline -->
<script src="<?=base_url('public/AdminLTE/plugins/sparklines/sparkline.js')?>"></script>

<!-- jQuery Knob Chart -->
<script src="<?=base_url('public/AdminLTE/plugins/jquery-knob/jquery.knob.min.js')?>"></script>
<!-- daterangepicker -->
<script src="<?=base_url('public/AdminLTE/plugins/moment/moment.min.js')?>"></script>
<script src="<?=base_url('public/AdminLTE/plugins/daterangepicker/daterangepicker.js')?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=base_url('public/AdminLTE/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')?>"></script>


<script src="<?=base_url('public/AdminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('public/AdminLTE/dist/js/adminlte.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="--><?//=base_url('public/AdminLTE/dist/js/pages/dashboard.js')?><!--"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('public/AdminLTE/dist/js/demo.js')?>"></script>

<script src="<?=base_url('public/plugins/Validation-Engine/jquery.validationEngine.js')?>"></script>
<script src="<?=base_url('public/plugins/Validation-Engine/jquery.validationEngine-en.js')?>"></script>

<script src="<?=base_url('public/AdminLTE/plugins/croppie/croppie.js')?>"></script>
<script src="<?=base_url('public/plugins/fancybox/dist/jquery.fancybox.min.js')?>"></script>
<script src="<?=base_url('public/js/admin-js.js')?>"></script>

</body>
</html>
