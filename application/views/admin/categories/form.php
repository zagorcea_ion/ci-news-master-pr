
<div class="card card-primary modal-form" style="padding: 0!important; width: 400px">
    <div class="card-header" style="border-radius: 0!important;">
        <h3 class="card-title"><?=!empty($id_cat) ? 'Edit' : 'Add' ?> category</h3>
    </div>

    <form id="category-form" action="<?=base_url('admin-panel/categories/store')?>" method="post">
        <?php if (!empty($id_cat)) { ?>
            <input type="hidden" name="id_cat" value="<?=$id_cat?>">
        <?php } ?>

        <div class="card-body">
            <div class="form-group">
                <label for="name">Category name</label>
                <input type="text" class="form-control validate[required]" id="name" name="name" value="<?=!empty($name) ? $name : ''?>" placeholder="Category name:">
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>


<script>
    var validation = $("#category-form").validationEngine('attach',{promptPosition : "bottomLeft", maxErrorsPerField: 1});

    $('#category-form').on('submit', function (e) {
        e.preventDefault();
        if(!validation.validationEngine('validate')){
            return false;
        }

        $.ajax({
            url : $(this).attr('action'),
            type : 'POST',
            dataType : 'JSON',
            data : $(this).serialize(),
            success : function(response){
                if(response.status == 'success'){
                    sweetalert(response.status, 'Success', response.message);
                    $.fancybox.close();
                    dt.draw(false);
                }

                if(response.status == 'error'){
                    sweetalert(response.status, 'Oops...', response.message);
                }
            }
        });
    });

</script>