<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_Model extends CI_Model
{
    private $tableName = 'contact';

    public $validationRules = [
        [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required'
        ],
        [
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required'
        ],
        [
            'field' => 'phone',
            'label' => 'Phone',
            'rules' => 'required'
        ],
        [
            'field' => 'message',
            'label' => 'Message',
            'rules' => 'required'
        ],
    ];

    /**
     * Validation rules
     * @return $this->validationRules
     */
    public function getValidationRules()
    {
        return $this->validationRules;
    }

	/**
	 * Add record
	 * @param array $data
	 * @return  $this->db->insert()
	 */
    public function add($data = [])
    {
        return $this->db->insert($this->tableName, $data);
    }

	/**
	 * Update record
	 * @param array $conditions
	 * @param $data
	 * @return  $this->db->update()
	 */
    public function update($conditions = [], $data)
    {
        extract($conditions);

        $this->db = $this->conditions($conditions);

        return $this->db->update($this->tableName, $data);
    }

	/**
	 * Delete record
	 * @param array $conditions
	 * @return  $this->db->delete()
	 */
    public function delete($conditions = [])
    {
        extract($conditions);

        $this->db = $this->conditions($conditions);

        return $this->db->delete($this->tableName);
    }

	/**
	 * Get all record
	 * @param array $conditions
	 * @return $this->db->get()->result_array()
	 */
    public function getAll($conditions = [])
    {
        $orderCol = 'date_add';
        $orderDir = 'DESC';

        extract($conditions);

        $this->db->from($this->tableName);
        $this->db->order_by($orderCol, $orderDir);
        $this->db = $this->conditions($conditions);

        return $this->db->get()->result_array();
    }

	/**
	 * Get one record
	 * @param array $conditions
	 * @return $this->db->row_array()
	 */
    public function getOne($conditions = [])
    {
        $this->db = $this->conditions($conditions);

        return $this->db->get($this->tableName)->row_array();
    }

	/**
	 * Count records
	 * @param array $conditions
	 * @return $this->db->count_all_results()
	 */
    public function count($conditions = [])
    {
        $this->db->from($this->tableName);
        $this->db = $this->conditions($conditions);

        return $this->db->count_all_results();
    }

	/**
	 * Check if record exists
	 * @param int $id
	 * @return $this->db->count_all_results()
	 */
    public function checkExists($id = 0)
    {
        $this->db->from($this->tableName);
        $this->db->where('id_contact', $id);
        return $this->db->count_all_results();
    }

	/**
	 * Generate all conditions
	 * @param array $conditions
	 * @return $this->db
	 */
    private function conditions($conditions = [])
    {
        extract($conditions);

        if (isset($id_contact)) {
            $this->db->where('id_contact', $id_contact);
        }

        return $this->db;
    }
}
