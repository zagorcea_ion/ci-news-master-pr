<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller
{

	private $perPage = 10;

	/**
     * Display home page
     */
	public function index()
	{
		//load model
		$this->load->model('News_Model', 'news');

		$params['join_categories'] = true;
		$params['limit'] = $this->perPage;
		$params['orderCol'] = 'date_publish';
		$params['orderDir'] = 'DESC';
		
		$data['news'] = $this->news->getAll($params);
		$data['most_popular'] = $this->news->getAll(['orderCol' => 'views', '' => 'DESC', 'limit' => 6]);
		$data['must_read'] = $this->news->getAll(['join_categories' => true, 'is_must_read' => true, 'limit' => 6]);
		// debug($data['must_read']);die;

		$this->load->view('layouts/public_header', $data);
		$this->load->view('home_page');
		$this->load->view('layouts/public_footer');
	}

	/**
     * Display contact page
     */
	public function contact()
	{
		$this->load->view('layouts/public_header');
		$this->load->view('contact_page');
		$this->load->view('layouts/public_footer');
	}

	/**
     * Display all news page
     */
	public function news()
	{
		//load model
		$this->load->model('News_Model', 'news');
		$this->load->library('pagination');

		$uri = $this->uri->uri_to_assoc(2);
		
		$params['join_categories'] = true;

		$config['base_url'] = base_url('news');

		if (!empty($uri['category'])) {
			$params['id_category'] = getIdUrl($uri['category']);
			$config['base_url'] = base_url('news/category/' . $uri['category']);
		}

		$config['prefix'] = '/page/';
        $config['total_rows'] = $this->news->count($params);
        $config['per_page'] = $this->perPage;
		$config['use_page_numbers'] = true;
		
		$config['first_link'] = false;
		$config['last_link'] = false;

        $config['full_tag_open'] = '<ul class="page-numbers">';
        $config['full_tag_close'] = '</ul>';

        $config['prev_tag_open'] = '<li class="prev page-numbers">';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';

        $config['next_tag_open'] = '<li class="next page-numbers">';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
		
        $config['num_tag_open'] = '<li class="page-numbers">';
        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li><span class="page-numbers current">';
        $config['cur_tag_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $params['offset'] = isset($uri['page']) ? (intval($uri['page']) - 1) * $this->perPage : '0';
		$params['limit'] = $config['per_page'];
		$params['orderCol'] = 'date_publish';
		$params['orderDir'] = 'DESC';

		$data['news'] = $this->news->getAll($params);
		$data['most_popular'] = $this->news->getAll(['orderCol' => 'views', '' => 'DESC', 'limit' => 6]);

		$this->load->view('layouts/public_header', $data);
		$this->load->view('news_page');
		$this->load->view('layouts/public_footer');
	}

	/**
     * Display news details page
	 * @param string $slug
     */
	public function newsDetail($slug)
	{
		if (empty($slug)) {
			redirect('404');
		}

		$idNews = getIdUrl($slug);

		//load model
		$this->load->model('News_Model', 'news');

		$data['news'] = $this->news->getOne(['id_news' => $idNews]);

		//Update views counter
		$this->news->incrementViews($idNews);

		$data['can_be_interesting'] = $this->news->getAll(['not_is' => $idNews, 'order_random' => true, 'limit' => 3]);
		$data['most_popular'] = $this->news->getAll(['orderCol' => 'views', 'orderDir' => 'DESC', 'limit' => 6]);

		$this->load->view('layouts/public_header', $data);
		$this->load->view('news_detail_page');
		$this->load->view('layouts/public_footer');
	}
	
	/**
     * Display 404 page
     */
    public function error404(){
		$this->load->view('layouts/public_header');
		$this->load->view('errors/html/error_404');
		$this->load->view('layouts/public_footer');
    }
}
