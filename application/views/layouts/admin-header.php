<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Znews | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('public/images/favicon.ico')?>">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('public/AdminLTE/plugins/fontawesome-free/css/all.min.css')?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="<?=base_url('public/AdminLTE/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url('public/AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css')?>">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('public/AdminLTE/dist/css/adminlte.min.css')?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?=base_url('public/AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">

    <link rel="stylesheet" href="<?=base_url('public/AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')?>">

    <link rel="stylesheet" href="<?=base_url('public/AdminLTE/plugins/croppie/croppie.css')?>" />

    <link rel="stylesheet" href="<?=base_url('public/plugins/fancybox/dist/jquery.fancybox.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('public/css/admin-css.css')?>" />
    <link rel="stylesheet" href="<?=base_url('public/css/sizes.css')?>" />


    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="<?=base_url('public/plugins/Validation-Engine/validationEngine.jquery.css')?>" rel="stylesheet" type="text/css" />
    <!-- jQuery -->
    <script src="<?=base_url('public/AdminLTE/plugins/jquery/jquery.min.js')?>"></script>

    <script src="<?=base_url('public/AdminLTE/plugins/datatables/jquery.dataTables.js')?>"></script>
    <script src="<?=base_url('public/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?=base_url('admin-panel/')?>" class="brand-link">
            <img src="<?=base_url('public/AdminLTE/dist/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light"><strong>Z</strong>NEWS</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
					<?php if (!empty($this->session->logged_user['avatar'])) { ?>
                    	<img src="<?=base_url('public/images/users/' . $this->session->logged_user['avatar'])?>" class="img-circle elevation-2" alt="<?=$this->session->logged_user['surname'] . ' ' . $this->session->logged_user['name']?>">
					<?php } else { ?>
						<img src="<?=base_url('public/images/no_avatar.png')?>" class="img-circle elevation-2" alt="<?=$this->session->logged_user['surname'] . ' ' . $this->session->logged_user['name']?>">
					<?php } ?>

                </div>
                <div class="info">
                    <a href="<?=base_url('admin-panel/users/profile')?>" class="d-block"><?=$this->session->logged_user['surname'] . ' ' . $this->session->logged_user['name']?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    <li class="nav-item">
                        <a href="<?=base_url('admin-panel/categories')?>" class="nav-link <?=!empty($activePage) && $activePage == 'categories' ? 'active' : ''?>">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Categories</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?=base_url('admin-panel/news')?>" class="nav-link <?=!empty($activePage) && $activePage == 'news' ? 'active' : ''?>">
                            <i class="nav-icon far fa-newspaper"></i>
                            <p>News</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?=base_url('admin-panel/contact')?>" class="nav-link <?=!empty($activePage) && $activePage == 'contact' ? 'active' : ''?>">
                            <i class="nav-icon fas fa-envelope"></i>                            
                            <p>Contact message</p>
                        </a>
                    </li>

                    <li class="nav-header">USER</li>

                    <li class="nav-item">
                        <a href="<?=base_url('admin-panel/users/profile')?>" class="nav-link <?=!empty($activePage) && $activePage == 'profile' ? 'active' : ''?>">
                            <i class="nav-icon far fa-user"></i>
                            <p>Profile</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="logout" data-text="Do you want to leave the admin-panel?" data-url="<?=base_url('admin-panel/users/logout')?>">
                            <i class="nav-icon fas fa-arrow-alt-circle-left"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>


