<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('User');
	}

	/**
	 * Display login page
	 */
	public function login()
	{
		$this->load->view('admin/users/login');
	}

	/**
	 * Authentication user
	 */
	public function authentication()
	{
		checkAjax();

		$post = $this->input->post(null, true);

		//load Model
		$this->load->model('User_Model', 'user');

		$user = $this->user->getOne(['login' => $post['login']]);

		if (empty($user)) {
			displayJson('error', 'Login or password is wrong!');
		}

		if (password_verify($post['password'], $user['password'])) {
			$this->session->set_userdata('logged_user', $user);
			displayJson('success', 'Success');

		} else {
			displayJson('error', 'Login or password is wrong!');
		}
	}

    /**
     * Display profile page
     */
    public function profile()
    {
		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
		}

		//load Model
		$this->load->model('User_Model', 'user');

		$user = $this->user->getOne(['id_user' => getUserData('id_user')]);

        $this->load->view('layouts/admin-header', ['activePage' => 'profile', 'record' => $user]);
        $this->load->view('admin/users/profile');
        $this->load->view('layouts/admin-footer');
    }

    /**
     * Update user
     */
    public function update()
    {
		checkAjax();

		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
		}

		//load model
		$this->load->model('User_Model', 'user');
		$this->load->library('form_validation');

		$this->form_validation->set_rules($this->user->getValidationRules());
        if (!$this->form_validation->run()) {
            displayJson('error', validation_errors());
        }

		$avatar = $this->input->post('avatar');
		$post = $this->input->post(null, true);

		if (!empty($avatar)) {
			$this->load->helper('My_file');

			$avatar = base64Save($avatar, 'public/images/users/');
			deleteFile('public/images/users/' . $post['old_avatar']);
		}

		$data = [
			'name'    => $post['name'],
			'surname' => $post['surname'],
			'login'   => $post['login'],
			'email'   => $post['email'],
			'avatar'  => !empty($avatar) ? $avatar : $post['old_avatar'],
		];

		if ($this->user->update(['id_user' => getUserData('id_user')], $data)) {
			displayJson('success', 'Profile data was successfully updated!');
		}
    }

	/**
	 * Change user password
	 */
	public function changePassword()
	{
		checkAjax();

		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
		}

		$post = $this->input->post(null, true);

		//load model
		$this->load->model('User_Model', 'user');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('password', 'Password', 'required|min_length[3]');
		if (!$this->form_validation->run()) {
			displayJson('error', validation_errors());
		}

		$user = $this->user->getOne(['id_user' => getUserData('id_user')]);

		if (!password_verify($post['old_password'], $user['password'])) {
			displayJson('error', 'Old password is wrong!');
		}

		$data['password'] = password_hash($post['password'], PASSWORD_DEFAULT);

		if ($this->user->update(['id_user' => getUserData('id_user')], $data)) {
			displayJson('success', 'Password was successfully updated!');
		}
	}

	/**
	 * Logout user
	 */
	public function logout()
	{
		$this->session->unset_userdata('logged_user');
		redirect(base_url());
	}
}
