<?php
/**
 * Check if users is logged
 */
function isLogged()
{
	$CI = &get_instance();

	if(isset($CI->session->logged_user)){
		return true;
	}

	return false;
}

/**
 * Get user data from session
 * @param string $key - data key
 * @return
 */
function getUserData($key = '')
{
	$CI = &get_instance();

	if (!empty($key)) {
		return $CI->session->logged_user[$key];

	} else {
		return $CI->session->logged_user;
	}
}

