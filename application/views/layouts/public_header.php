<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title> Znews - IT news </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Place favicon.ico in the root directory -->
<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('public/images/favicon.ico')?>">

<!-- All css files are included here. -->
<!-- Bootstrap fremwork main css -->
<link rel="stylesheet" href="<?=base_url('public/css/bootstrap.min.css')?>">
<!-- This core.css file contents all plugings css file. -->
<link rel="stylesheet" href="<?=base_url('public/css/core.css')?>">
<!-- Theme shortcodes/elements style -->
<link rel="stylesheet" href="<?=base_url('public/css/shortcode/shortcodes.css')?>">
<!-- Theme main style -->
<link rel="stylesheet" href="<?=base_url('public/css/style.css')?>">
<!-- Responsive css -->
<link rel="stylesheet" href="<?=base_url('public/css/responsive.css')?>">
<!-- User style -->
<link rel="stylesheet" href="<?=base_url('public/css/custom.css')?>">

<link href="<?=base_url('public/plugins/Validation-Engine/validationEngine.jquery.css')?>" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?=base_url('public/AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')?>">

<!-- jQuery -->
<script src="<?=base_url('public/AdminLTE/plugins/jquery/jquery.min.js')?>"></script>

<!-- Modernizr JS -->
<script src="<?=base_url('public/js/vendor/modernizr-2.8.3.min.js')?>"></script>
</head>
<body>

<!--  THEME PRELOADER AREA -->
<div id="preloader-wrapper">
<div class="preloader-wave-effect"></div>
</div>
<!-- THEME PRELOADER AREA END -->

<!-- Body main wrapper start -->
<div class="wrapper">
<!-- Start of header area -->
<header  class="header-area header-wrapper bg-white clearfix">
	<!-- Start Sidebar Menu -->
	<div class="sidebar-menu">
		<div class="sidebar-menu-inner"></div>
		<span class="fa fa-remove"></span>
	</div>
	<!-- End Sidebar Menu -->

	<div class="header-middle-area">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-lg-4 col-sm-5 col-xs-12 header-mdh">
					<div class="global-table">
						<div class="global-row">
							<div class="global-cell">
								<div class="logo">
									<a href="<?=base_url()?>">
										<img src="<?=base_url('public/images/logo/1.png')?>" alt="main logo">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div id="sticky-header" class="header-bottom-area hidden-sm hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="menu-wrapper  bg-theme clearfix">
						<div class="row">
							<div class="col-md-11">
								<div class="mainmenu-area">
									<nav class="primary-menu uppercase">
										<ul class="clearfix">
											<li><a href="<?=base_url()?>">Home</a></li>
											<li><a href="<?=base_url('news')?>">News</a></li>
											<li><a href="<?=base_url('contact')?>">Contact</a></li>
										</ul>
									</nav>
								</div>
							</div>
					
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- mobile-menu-area start -->
	<div class="mobile-menu-area hidden-md hidden-lg">
		<div class="fluid-container">
				<nav id="mobile_dropdown">
					<ul>
						<li><a href="<?=base_url()?>">Home</a></li>
						<li><a href="<?=base_url('news')?>">News</a></li>
						<li><a href="<?=base_url('contact')?>">Contact</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
	<!-- mobile-menu-area end -->
</header>
<!-- End of header area -->
