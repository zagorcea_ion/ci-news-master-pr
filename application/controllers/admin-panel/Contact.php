<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller
{
    /**
     * Display admin contact page
     */
    public function index()
    {
        $this->load->helper('User');

		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
        }
        
        $this->load->view('layouts/admin-header', ['activePage' => 'contact']);
        $this->load->view('admin/contact/index');
        $this->load->view('layouts/admin-footer');
    }

    /**
     * Select all records for Data Table plugin
     */
    public function dtContact()
    {
        $this->load->helper('User');

		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
        }
        
		checkAjax();

        $columns = [
            5 => 'date_add',
        ];

        $post = $this->input->post();
        $this->load->model('Contact_Model', 'contact');

        //Config search data
        $search = $post['search']['value'];

        if (!empty($search)) {
            $params['keywords'] =  $search;
        }

        $params['offset'] = intval($post['start']);
        $params['limit'] = intval($post['length']);
        $params['orderDir'] = $post['order'][0]['dir'];
        $params['orderCol'] =  $columns[intval($post['order'][0]['column'])];

        $contact = $this->contact->getAll($params);

        $data = [];

        if (!empty($contact)) {
            foreach ($contact as $key => $item) {
                $data[$key] = [
                    'name'     => $item['name'],
                    'email'    => $item['email'],
                    'phone'    => $item['phone'],
                    'website'  => $item['website'],
                    'message'  => $item['message'],
                    'date_add' => date('d.m.Y', strtotime($item['date_add'])),
                    
                    'viewed'   => '<input class="change-flag" id="viewed-' .  $item['id_contact'] . '" data-url="' .  base_url('admin-panel/contact/is-viewed/' . $item['id_contact']) . '" type="checkbox" ' . ($item['viewed'] ? "checked" : " ") . '>
                                        <label class="switch-color" for="viewed-' .  $item['id_contact'] . '"></label>',
                    
                    'action'   => '<button type="button" class="btn btn-danger confirm" data-text="Do you want delete this contact?" data-url="' . base_url('admin-panel/contact/delete/' . $item['id_contact']) . '"><i class="fas fa-trash-alt"></i> Delete</button>'
                ];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = $this->contact->count();
        $output['recordsFiltered'] = $this->contact->count($params);

        echo json_encode($output);
    }


    /**
     * Store contact message
     */
    public function store()
    {
		checkAjax();

        $post = $this->input->post();

        $this->load->library('form_validation');
        $this->load->model('Contact_Model', 'contact');

        $this->form_validation->set_rules($this->contact->getValidationRules());
        if(!$this->form_validation->run()){
            displayJson('error', validation_errors());
        }

        $data = [
        	'name'     => $post['name'],
			'email'    => $post['email'],
			'phone'    => $post['phone'],
			'website'  => $post['website'],
			'message'  => $post['message'],
			'date_add' => date('Y-m-d H:i:s'),
		];

        if (!$this->contact->add($data)) {
            displayJson('error', 'Contact message was not added');
        }

        displayJson('success', 'Contact message was added successfully');
    }

    /**
	 * Delete contact
	 * @param int $id
	 */
    public function delete($id = 0)
    {
        $this->load->helper('User');

		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
        }
        
		checkAjax();

        $id = intval($id);

        $this->load->model('Contact_Model', 'contact');

        if (!$this->contact->checkExists($id)) {
            displayJson('error', 'Contact was not found');
        }

        if (!$this->contact->delete(['id_contact' => $id])) {
            displayJson('error', 'Contact was not delete successfully');
        }

        displayJson('success', 'Contact was delete successfully');
    }

    /**
	 * Make contact viewed
	 * @param int $id
	 */
	public function isViewed($id = 0)
	{
        $this->load->helper('User');

		if (!isLogged()) {
			redirect(base_url('/admin-panel/login'));
        }
        
		checkAjax();

		$id = intval($id);

		//load Model
		$this->load->model('Contact_Model', 'contact');

		$contact = $this->contact->getOne(['id_contact' => $id]);

		if (empty($contact)) {
			displayJson('error', 'Contact was not found');
		}

		if ($contact['viewed']) {
			$data['viewed'] = 0;
			$message = 'Contact is not "viewed"';

		} else {
			$data['viewed'] = 1;
			$message = 'Contact was made "viewed"';
		}

		if ($this->contact->update(['id_contact' => $id], $data)) {
			displayJson('success', $message);
		}
	}
}