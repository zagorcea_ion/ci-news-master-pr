<a href="<?= base_url('admin-panel/news/form/' . $id_news) ?>" class="btn btn-primary">
	<i class="fas fa-edit"></i> Edit
</a>

<button type="button" class="btn btn-danger confirm" data-text="Do you want delete this news?" data-url="<?= base_url('admin-panel/news/delete/' . $id_news) ?>">
	<i class="fas fa-trash-alt"></i> Delete
</button>
