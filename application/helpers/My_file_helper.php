<?php

/**
 * Save image
 * @param $savePath - path to save
 * @param string $width - image width for resize
 * @param string $height - image height for resize
 * @return array $uploadedImage['file_name']
 */
function saveImage($savePath, $width = '', $height = '')
{
    $CI =& get_instance();
    $config = [
        'upload_path'   => FCPATH . $savePath,
        'allowed_types' => 'gif|jpg|jpeg|png',
        'max_size'      => 1024,
        'file_name'     => uniqid(),
    ];

    $CI->load->library('upload', $config);

    if (!$CI->upload->do_upload('image')) {
        return ['error' => $CI->upload->display_errors()];
    }

    $uploadedImage = $CI->upload->data();

    if (!empty($width) && !empty($height)) {
        resizeImage($savePath . '/' . $uploadedImage['file_name'], $width, $height);
    }

    return $uploadedImage['file_name'];
}

/**
 * Resize image
 * @param $filePath - path to file
 * @param $width - image width for resize
 * @param $height - image height for resize
 * @return true or errors array
 */
function resizeImage($filePath, $width, $height)
{
    $CI =& get_instance();
    $config = [
        'image_library'  => 'gd2',
        'source_image'   => $filePath,
        'new_image'      => $filePath,
        'create_thumb'   => false,
        'maintain_ratio' => true,
        'width'          => $width,
        'height'         => $height
    ];

    $CI->load->library('image_lib', $config);

    if (!$CI->image_lib->resize()){
        return ['error' => $CI->image_lib->display_errors()];
    }

    return true;
}

/**
 * Delete file
 * @param $filePath - path to file
 * @return true or false
 */
function deleteFile($filePath)
{
    $filePath = FCPATH  . $filePath;

    if (file_exists($filePath)) {
       return unlink($filePath);
    }

    return false;
}

/**
 * Save base64 as file
 * @param $image - image on base64
 * @param string $path - save path
 * @return file name
 */
function base64Save($image, $path = 'public/images/')
{
	$image_name = uniqid() . '.jpeg';

	$image_file = fopen(FCPATH . $path . $image_name, 'wb');
	$data = explode(',', $image);
	fwrite($image_file, base64_decode($data[1]));
	fclose($image_file);

	return $image_name;
}
